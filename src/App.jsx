import {ComponenteDos}  from "./Components/ComponenteDos"
import ComponenteUno from "./Components/ComponenteUno"



function App() {

  return (
    <div className=""> 
    
        <h1>Hola mundo desde Riact</h1>

        <ComponenteUno />
        <ComponenteDos />

    </div>
    
  )
}

export default App
